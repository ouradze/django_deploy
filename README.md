#### Setup:

1) Clone/download repo.
2) Create a virtualenv and use [poetry](https://poetry.eustace.io/docs/) to install the dependencies
3) Use `invoke build` command to build your image
4) Run migrations with `invoke run-migrations`
5) Run the dev server with `invoke dev`

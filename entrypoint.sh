#!/bin/sh

until python manage.py migrate
do
	echo "Waiting for postgres to be ready..."
	sleep 2
done

exec "$@"

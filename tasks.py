import os
import textwrap

from invoke import task


@task
def black(c):
    print("Running black check")
    c.run("black --check .")


@task
def flake8(c):
    print("Running flake8 check")
    c.run("flake8 .")


@task
def isort(c):
    print("Running isort check")
    c.run("isort -c")


@task(black, flake8, isort)
def quality(c):
    print("Quality checks successful")


@task
def build(c, scratch=False):
    if scratch:
        c.run("docker-compose -p django_deploy build --no-cache")

    c.run("docker-compose -p django_deploy build")


@task
def run(c):
    c.run("docker-compose -p django_deploy up -d")


@task
def stop(c):
    c.run("docker-compose -p django_deploy down")


@task
def dev(c):
    c.run("cp env/dev.env .env")
    c.run("docker-compose -p django_deploy up")


@task
def manage(c, args):
    c.run(f"docker-compose run --rm app python manage.py {args}")


@task
def tests(c, args=""):
    if not os.environ.get("CI_JOB_TOKEN") or os.environ.get("RUN_IN_DOCKER"):
        c.run("cp env/dev.env .env")
        c.run(f"docker-compose run --rm app pytest -s {args}", pty=True)
    else:
        c.run(f"pytest {args}")


@task
def setup_env(c):
    c.run("echo 'Writing production configuration to file'")
    config = f"""\
        # compose setup
        PRODUCTION_IMAGE={os.environ["PRODUCTION_IMAGE"]}:{os.environ["IMAGE_TAG"]}
        NGINX_IMAGE={os.environ["NGINX_IMAGE"]}:{os.environ["IMAGE_TAG"]}

        # django setup
        DEBUG=false
        PRODUCTION=true
        SECRET_KEY="{os.environ["SECRET_KEY_PROD"]}"
        DATABASE_URL={os.environ["DATABASE_URL_PROD"]}
        ALLOWED_HOSTS={os.environ["ALLOWED_HOSTS_PROD"]}
    """

    with open(".env", "w") as fp:
        fp.write(textwrap.dedent(config))


@task
def restart_service(c):
    script = """\
        set -x
        cd /app
        export $(cat .env | grep -v '^#' | xargs)
        docker pull $PRODUCTION_IMAGE
        docker pull $NGINX_IMAGE
        docker-compose up -d
    """
    script.replace("\n", ";")
    ssh_command = f"ssh -o StrictHostKeyChecking=no -o ControlMaster=auto -o ControlPersist=3s root@$DIGITAL_OCEAN_IP_ADDRESS -- bash -c '{script}'"
    c.run("echo 'Restarting service...'")
    c.run(
        "scp -o StrictHostKeyChecking=no -r ./docker-compose.yml root@$DIGITAL_OCEAN_IP_ADDRESS:/app"
    )
    c.run(ssh_command)
    c.run("echo 'Service restarted'")


@task(pre=[setup_env], post=[restart_service])
def deploy(c):
    c.run("echo 'Starting configuration deployment'")
    c.run(
        "scp -o StrictHostKeyChecking=no -r ./.env root@$DIGITAL_OCEAN_IP_ADDRESS:/app"
    )
    c.run("echo 'Configuration deployment successful'")

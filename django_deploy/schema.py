import graphene

from django.conf import settings

from graphene_django.debug import DjangoDebug

from django_deploy.todos import schema as todo_schema
from django_deploy.users import schema as users_schema


class RootQuery(todo_schema.Queries, users_schema.Queries, graphene.ObjectType):
    def __init__(self, **kwargs):
        if settings.DEBUG:
            self.debug = graphene.Field(DjangoDebug, name="__debug")
        super().__init__(**kwargs)


class RootMutations(todo_schema.Mutations, users_schema.Mutations, graphene.ObjectType):
    pass


#  schema = graphene.Schema(query=RootQuery, mutation=RootMutations)
schema = graphene.Schema(query=RootQuery)

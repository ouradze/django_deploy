import graphene

from graphene_django.types import DjangoObjectType

from django_deploy.todos import models


class TodoType(DjangoObjectType):
    class Meta:
        model = models.Todo


class TodoListType(DjangoObjectType):
    class Meta:
        model = models.TodoList


class Queries:
    todo = graphene.Field(TodoType, id=graphene.Int())
    todos = graphene.List(TodoType)

    todo_list = graphene.Field(TodoListType, id=graphene.Int())
    todo_lists = graphene.List(TodoListType)

    def resolve_todo(self, info, **kwargs):
        todo_id = kwargs.get("id")

        if todo_id is not None:
            return models.Todo.objects.get(pk=todo_id)

        return None

    def resolve_todo_list(self, info, **kwargs):
        todo_list_id = kwargs.get("id")

        if todo_list_id is not None:
            return models.TodoList.objects.get(pk=todo_list_id)

        return None

    def resolve_todos(self, info, **kwargs):
        return models.Todo.objects.all()

    def resolve_todo_lists(self, info, **kwargs):
        return models.TodoList.objects.all()


class Mutations:
    pass

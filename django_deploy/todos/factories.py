from django.utils import timezone

import factory

from django_deploy.users import factories as users_factories

from . import models


class TodoListFactory(factory.django.DjangoModelFactory):
    title = factory.Sequence(lambda n: "Awesome title%d" % n)
    created_at = factory.LazyFunction(timezone.now)
    creator = factory.SubFactory(users_factories.UserFactory)

    class Meta:
        model = models.TodoList


class TodoFactory(factory.django.DjangoModelFactory):
    description = factory.Sequence(lambda n: "Awesome title%d" % n)
    created_at = factory.LazyFunction(timezone.now)
    assigned_to = factory.SubFactory(users_factories.UserFactory)
    todolist = factory.SubFactory(TodoListFactory)

    class Meta:
        model = models.Todo

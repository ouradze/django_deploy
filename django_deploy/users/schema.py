import graphene

from django.contrib.auth.models import User

from graphene_django import DjangoObjectType


class UserType(DjangoObjectType):
    class Meta:
        model = User
        description = "Type definition for a single user"


class Queries:
    user = graphene.Field(UserType, user_id=graphene.Int())
    users = graphene.List(UserType, description="User List")

    def resolve_user(self, info, **kwargs):
        user_id = kwargs.get("user_id")

        if user_id:
            return User.objects.get(pk=user_id)
        return None

    def resolve_users(self, info, **kwargs):
        return User.objects.all()


class Mutations:
    pass

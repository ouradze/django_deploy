from django.contrib.auth.models import User

import factory


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence("eclar_{0}".format)
    first_name = factory.Sequence("Mehdi {0}".format)
    last_name = factory.Sequence("Lee {0}".format)
    email = factory.Sequence("mehdi.lee{0}@domain.com".format)

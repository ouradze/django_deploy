FROM python:3.7.2-alpine as python-base
ENV PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_PATH=/opt/poetry \
    PYTHONUNBUFFERED=1 \
    VENV_PATH=/opt/venv \
    # we don't want those pesky pyc files
    PYTHONDONTWRITEBYTECODE=1
ENV PATH="$POETRY_PATH/bin:$VENV_PATH/bin:$PATH"
RUN apk add libpq
# set work directory
WORKDIR /app


FROM python-base as builder
# Install poetry package manager and dependencies
RUN apk add --no-cache gettext postgresql-dev ca-certificates gcc libffi-dev jpeg-dev zlib-dev musl-dev python-dev curl bash \
    && curl -ssL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python \
    && mv /root/.poetry $POETRY_PATH \
    && poetry --version \
    # configure poetry & make a virtualenv ahead of time since we only need one
    && python -m venv $VENV_PATH \
    && poetry config settings.virtualenvs.create false \
    # cleanup
    && rm -rf /var/lib/apt/lists/*

COPY poetry.lock pyproject.toml ./
RUN poetry install --no-dev --no-interaction --no-ansi -vvv


FROM builder as dev
RUN poetry install --no-interaction --no-ansi -vvv
COPY . /app
ENTRYPOINT ["/app/entrypoint.sh"]


FROM python-base as prod

COPY --from=builder $VENV_PATH $VENV_PATH
COPY ./django_deploy ./django_deploy
COPY ./entrypoint.sh manage.py ./
RUN SECRET_KEY=fake-secret-key ALLOWED_HOSTS="[*]" DATABASE_URL="sqlite://" python manage.py collectstatic --no-input --clear
ENTRYPOINT ["/app/entrypoint.sh"]

import json

from graphene_django.utils.testing import GraphQLTestCase

from django_deploy.schema import schema
from django_deploy.todos import factories as todos_factories


class ApiTestCase(GraphQLTestCase):
    GRAPHQL_SCHEMA = schema

    def setUp(self):
        super().setUp()
        self.todo_list = todos_factories.TodoListFactory()
        self.todo = todos_factories.TodoFactory(todolist=self.todo_list)

        self.todo_list2 = todos_factories.TodoListFactory()
        self.todo2 = todos_factories.TodoFactory(todolist=self.todo_list2)

    def gql_response(self, query, op_name, data):
        response = self.query(query, op_name=op_name)
        content = json.loads(response.content)
        # This validates the status code and if you get errors
        self.assertResponseNoErrors(response)
        self.assertDictEqual(content, data)

    def test_get_todo(self):
        todo_id = self.todo.pk
        query = (
            """
            query {
                todo(id: %s) {
                id
                description
                createdAt
                assignedTo {
                  id
                }
                todolist {
                  id
                }
              }
            }
            """
            % todo_id
        )
        expected_data = {
            "data": {
                "todo": {
                    "id": str(self.todo.pk),
                    "createdAt": str(self.todo.created_at),
                    "description": self.todo.description,
                    "todolist": {"id": str(self.todo_list.pk)},
                    "assignedTo": {"id": str(self.todo.assigned_to.pk)},
                }
            }
        }
        self.gql_response(query, "todo", expected_data)

    def test_get_todos(self):
        query = """
            query {
                todos {
                id
                createdAt
                description
                todolist {
                  id
                }
                assignedTo {
                  id
                }
              }
            }
            """
        expected_data = {
            "data": {
                "todos": [
                    {
                        "id": str(self.todo.pk),
                        "createdAt": str(self.todo.created_at),
                        "description": self.todo.description,
                        "todolist": {"id": str(self.todo_list.pk)},
                        "assignedTo": {"id": str(self.todo.assigned_to.pk)},
                    },
                    {
                        "id": str(self.todo2.pk),
                        "createdAt": str(self.todo2.created_at),
                        "description": self.todo2.description,
                        "todolist": {"id": str(self.todo_list2.pk)},
                        "assignedTo": {"id": str(self.todo2.assigned_to.pk)},
                    },
                ]
            }
        }
        self.gql_response(query, "todos", expected_data)

    def test_get_todo_list(self):
        todo_list_id = self.todo_list.pk
        query = (
            """
            query {
                todoList(id: %s) {
                    id
                    title
                    createdAt
                    creator {
                      id
                    }
                    todos {
                      id
                    }
                }
            }
            """
            % todo_list_id
        )
        expected_data = {
            "data": {
                "todoList": {
                    "id": str(self.todo_list.pk),
                    "createdAt": str(self.todo_list.created_at),
                    "title": self.todo_list.title,
                    "creator": {"id": str(self.todo_list.creator.pk)},
                    "todos": [{"id": str(self.todo.pk)}],
                }
            }
        }
        self.gql_response(query, "todoList", expected_data)

    def test_get_todo_lists(self):
        query = """
            query {
                todoLists {
                    id
                    title
                    createdAt
                    creator {
                      id
                    }
                    todos {
                      id
                    }
                }
            }
            """
        expected_data = {
            "data": {
                "todoLists": [
                    {
                        "id": str(self.todo_list.pk),
                        "createdAt": str(self.todo_list.created_at),
                        "title": self.todo_list.title,
                        "creator": {"id": str(self.todo_list.creator.pk)},
                        "todos": [{"id": str(self.todo.pk)}],
                    },
                    {
                        "id": str(self.todo_list2.pk),
                        "createdAt": str(self.todo_list2.created_at),
                        "title": self.todo_list2.title,
                        "creator": {"id": str(self.todo_list2.creator.pk)},
                        "todos": [{"id": str(self.todo2.pk)}],
                    },
                ]
            }
        }
        self.gql_response(query, "todoLists", expected_data)
